\select@language {spanish}
\contentsline {chapter}{Tabla de contenido}{1}{chapter*.1}
\contentsline {chapter}{Lista de Figura}{2}{chapter*.1}
\contentsline {chapter}{\numberline {1}PROPUESTA DE TRABAJO DE GRADO}{3}{chapter.1}
\contentsline {section}{\numberline {1}DESCRIPCI\IeC {\'O}N GENERAL DEL PROYECTO}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1}T\IeC {\'\i }tulo del Proyecto de Grado}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.2}Direcci\IeC {\'o}n de Ejecuci\IeC {\'o}n}{3}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.3}Lapso de Ejecuci\IeC {\'o}n}{3}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.4}Organismo o Instituci\IeC {\'o}n Responsable del Proyecto}{4}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.5}Informaci\IeC {\'o}n de Contacto de los Estudiantes}{4}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.6}L\IeC {\'\i }nea y Subl\IeC {\'\i }nea de Investigaci\IeC {\'o}n del Proyecto}{4}{subsection.1.1.6}
\contentsline {section}{\numberline {2}DESCRIPCI\IeC {\'O}N SITUACIONAL}{5}{section.1.2}
\contentsline {subsection}{\numberline {2.1}ESTADO DEL ARTE}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {2.2}PLANTEAMIENTO DEL PROBLEMA}{11}{subsection.1.2.2}
\contentsline {subsection}{\numberline {2.3}IMPACTO DEL PROBLEMA}{13}{subsection.1.2.3}
\contentsline {subsection}{\numberline {2.4}ANALISIS DE PARTICIPACI\IeC {\'O}N}{14}{subsection.1.2.4}
\contentsline {subsection}{\numberline {2.5}OBJETIVOS DEL PROYECTO}{14}{subsection.1.2.5}
\contentsline {subsection}{\numberline {2.6}JUSTIFICACI\IeC {\'O}N DEL PROYECTO}{15}{subsection.1.2.6}
\contentsline {subsection}{\numberline {2.7}Cronograma}{17}{subsection.1.2.7}
\contentsline {chapter}{ Referencias Bibliograf\IeC {\'\i }a}{19}{chapter*.4}
